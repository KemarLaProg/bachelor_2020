import 'vtk.js/Sources/favicon';

import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkCubeSource from 'vtk.js/Sources/Filters/Sources/CubeSource';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';

import vtkOpenGLRenderWindow from 'vtk.js/Sources/Rendering/OpenGL/RenderWindow';
import vtkRenderWindow from 'vtk.js/Sources/Rendering/Core/RenderWindow';
import vtkRenderWindowInteractor from 'vtk.js/Sources/Rendering/Core/RenderWindowInteractor';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';

import vtkOrientationMarkerWidget from 'vtk.js/Sources/Interaction/Widgets/OrientationMarkerWidget';
import vtkAnnotatedCubeActor from 'vtk.js/Sources/Rendering/Core/AnnotatedCubeActor';

import vtkSTLReader from 'vtk.js/Sources/IO/Geometry/STLReader'

import macro from 'vtk.js/Sources/macro';
import HttpDataAccessHelper from 'vtk.js/Sources/IO/Core/DataAccessHelper/HttpDataAccessHelper';
import vtkBoundingBox from 'vtk.js/Sources/Common/DataModel/BoundingBox';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
//import vtkVolumeController from 'vtk.js/Sources/Interaction/UI/VolumeController';
import vtkURLExtract from 'vtk.js/Sources/Common/Core/URLExtract';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkXMLImageDataReader from 'vtk.js/Sources/IO/XML/XMLImageDataReader';



const vtiReader = vtkXMLImageDataReader.newInstance();

const reader = vtkSTLReader.newInstance();
// ----------------------------------------------------------------------------
// Standard rendering code setup
// ----------------------------------------------------------------------------
const renderWindow = vtkRenderWindow.newInstance();
const renderer = vtkRenderer.newInstance({ background: [0.15, 0.17, 0.2] });
renderWindow.addRenderer(renderer);

// ----------------------------------------------------------------------------
// Simple pipeline ConeSource --> Mapper --> Actor
// ----------------------------------------------------------------------------
//const coneSource = vtkConeSource.newInstance({ height: 1.0 });

var actors = [];

//full domain
let fdSize = 10;
let fdPos = fdSize/2;

var cubeSource = vtkCubeSource.newInstance({ xLength: fdSize, yLength: fdSize, zLength: fdSize, center: [fdPos, fdPos, fdPos] });
var mapper = vtkMapper.newInstance();
var actor = vtkActor.newInstance();

actor.setMapper(mapper);
mapper.setInputConnection(cubeSource.getOutputPort());

//actor.getProperty().setOpacity(0.1);
//actor.getProperty().setRepresentationToWireframe();

const fd_pipeline = {cubeSource, mapper, actor};

// Create red wireframe baseline
fd_pipeline.actor.getProperty().setRepresentation(1);
fd_pipeline.actor.getProperty().setColor(1, 0, 0);

// ----------------------------------------------------------------------------
// Add the actor to the renderer and set the camera based on it
// ----------------------------------------------------------------------------
renderer.addActor(actor);
renderer.resetCamera();


// ----------------------------------------------------------------------------
// Use OpenGL as the backend to view the all this
// ----------------------------------------------------------------------------
const openglRenderWindow = vtkOpenGLRenderWindow.newInstance();
renderWindow.addView(openglRenderWindow);

// ----------------------------------------------------------------------------
// Create a div section to put this into
// ----------------------------------------------------------------------------
const container = document.createElement('div');
document.querySelector('#canvas').appendChild(container);
openglRenderWindow.setContainer(container);

// ----------------------------------------------------------------------------
// Capture size of the container and set it to the renderWindow
// ----------------------------------------------------------------------------
const { width, height } = document.querySelector('#canvas').getBoundingClientRect();
openglRenderWindow.setSize(width, height);

// ----------------------------------------------------------------------------
// Setup an interactor to handle mouse events
// ----------------------------------------------------------------------------
const interactor = vtkRenderWindowInteractor.newInstance();
interactor.setView(openglRenderWindow);
interactor.initialize();
interactor.bindEvents(container);


init_orientation_marker();

// --- render ---
renderer.addActor(actor);
renderWindow.render();

// ----------------------------------------------------------------------------
// Setup interactor style to use
// ----------------------------------------------------------------------------
const trackball = vtkInteractorStyleTrackballCamera.newInstance();
interactor.setInteractorStyle(trackball);

export function add_box(coord){

  pos_translate(coord, function(pos){

    mapper = vtkMapper.newInstance();
    actor = vtkActor.newInstance();
    cubeSource = vtkCubeSource.newInstance({ xLength: pos[0], yLength: pos[2], zLength: pos[4], center: [pos[1], pos[3], pos[5]]});

    actors.push(actor)

    actor.setMapper(mapper);
    mapper.setInputConnection(cubeSource.getOutputPort());

    actor.getProperty().setOpacity(0.5);
    /*
    actors[i].getProperty().setColor( files[i].color );
    actors[i].getProperty().setOpacity( files[i].opacity );
    */
    renderer.addActor(actor);

    //renderer.resetCamera();
    renderWindow.render();
  })
}

export function remove_box(id){
  renderer.removeActor(actors[id])

  actors.splice(id, 1);


  renderWindow.render();
}

export function toggle_box_visibility(id, callback){
  var d = actors[id]

  if (d.get().visibility == true) {
    d.set({visibility: false});

    renderWindow.render();

    callback(false)

  } else {
    d.set({visibility: true});

    renderWindow.render();

    callback(true)
  }
}

export function update_fulldomain(param, origin, length){
  var center = fd_pipeline.cubeSource.get().center;

  convert_pos(origin, length, function(o, l){

    switch (param) {
      case "x0":
        center[0] = o;
        fd_pipeline.cubeSource.set({xLength: l, center: center});
        break;
      case "x1":
        center[0] = o;
        fd_pipeline.cubeSource.set({xLength: l, center: center});
        break;
      case "y0":
        center[1] = o;
        fd_pipeline.cubeSource.set({yLength: l, center: center});
        break;
      case "y1":
        center[1] = o;
        fd_pipeline.cubeSource.set({yLength: l, center: center});
        break;
      case "z0":
        center[2] = o;
        fd_pipeline.cubeSource.set({zLength: l, center: center});
        break;
      case "z1":
        center[2] = o;
        fd_pipeline.cubeSource.set({zLength: l, center: center});
        break;
      default:

    }
    renderer.resetCameraClippingRange();
    renderWindow.render();
  })
}

function pos_translate(pos, callback){

  var xlength = pos[1] - pos[0];
  var xcenter = pos[0] + xlength/2;

  var ylength = pos[3] - pos[2];
  var ycenter = pos[2] + ylength/2;

  var zlength = pos[5] - pos[4];
  var zcenter = pos[4] + zlength/2;


  callback([xlength, xcenter, ylength, ycenter, zlength, zcenter])
}

function convert_pos(origin, length, callback){
  var l = length - origin;
  var o = origin + l/2;

  callback(o, l);
}


function init_orientation_marker(){
  // create axes
  const axes = vtkAnnotatedCubeActor.newInstance();
  axes.setDefaultStyle({
    text: '+X',
    fontStyle: 'bold',
    fontFamily: 'Arial',
    fontColor: 'black',
    fontSizeScale: (res) => res / 2,
    faceColor: '#9999ff',
    faceRotation: 0,
    edgeThickness: 0.1,
    edgeColor: 'black',
    resolution: 400,
  });
  // axes.setXPlusFaceProperty({ text: '+X' });
  axes.setXMinusFaceProperty({
    text: '-X',
    faceColor: '#ffff99',
    faceRotation: 90,
    fontStyle: 'italic',
  });
  axes.setYPlusFaceProperty({
    text: '+Y',
    faceColor: '#99ff99',
    fontSizeScale: (res) => res / 4,
  });
  axes.setYMinusFaceProperty({
    text: '-Y',
    faceColor: '#99ffff',
    fontColor: 'white',
  });
  axes.setZPlusFaceProperty({
    text: '+Z',
    edgeColor: 'yellow',
  });
  axes.setZMinusFaceProperty({ text: '-Z', faceRotation: 45, edgeThickness: 0 });

  // create orientation widget
  const orientationWidget = vtkOrientationMarkerWidget.newInstance({
    actor: axes,
    interactor: renderWindow.getInteractor(),
  });
  orientationWidget.setEnabled(true);
  orientationWidget.setViewportCorner(
    vtkOrientationMarkerWidget.Corners.BOTTOM_RIGHT
  );
  orientationWidget.setViewportSize(0.15);
  orientationWidget.setMinPixelSize(100);
  orientationWidget.setMaxPixelSize(300);
}


// ----------------------------------------------------------------------------
function update() {
  actor = vtkActor.newInstance();
  mapper = vtkMapper.newInstance();

  actor.setMapper(mapper);
  mapper.setInputConnection(reader.getOutputPort());

  const resetCamera = renderer.resetCamera;
  const render = renderWindow.render;

  renderer.addActor(actor);
  resetCamera();
  render();
}



function create_volume(fileContents){
  vtiReader.parseAsArrayBuffer(fileContents);

  const volume = vtiReader.getOutputData(0);
  const volumeMapper = vtkVolumeMapper.newInstance();
  const volumeActor = vtkVolume.newInstance();

  const dataArray = volume.getPointData().getScalars() || volume.getPointData().getArrays()[0];
  const dataRange = dataArray.getRange();

  const lookupTable = vtkColorTransferFunction.newInstance();
  const piecewiseFunction = vtkPiecewiseFunction.newInstance();

  // Pipeline handling
  volumeActor.setMapper(volumeMapper);
  volumeMapper.setInputData(volume);
  renderer.addActor(volumeActor);

  // Configuration
  const sampleDistance =
    0.7 *
    Math.sqrt(
      volume
        .getSpacing()
        .map((v) => v * v)
        .reduce((a, b) => a + b, 0)
    );
  mapper.setSampleDistance(sampleDistance);
  volumeActor.getProperty().setRGBTransferFunction(0, lookupTable);
  volumeActor.getProperty().setScalarOpacity(0, piecewiseFunction);
  // volumeActor.getProperty().setInterpolationTypeToFastLinear();
  volumeActor.getProperty().setInterpolationTypeToLinear();

  // For better looking volume rendering
  // - distance in world coordinates a scalar opacity of 1.0
  volumeActor
    .getProperty()
    .setScalarOpacityUnitDistance(
      0,
      vtkBoundingBox.getDiagonalLength(volume.getBounds()) /
        Math.max(...volume.getDimensions())
    );
  // - control how we emphasize surface boundaries
  //  => max should be around the average gradient magnitude for the
  //     volume or maybe average plus one std dev of the gradient magnitude
  //     (adjusted for spacing, this is a world coordinate gradient, not a
  //     pixel gradient)
  //  => max hack: (dataRange[1] - dataRange[0]) * 0.05
  volumeActor.getProperty().setGradientOpacityMinimumValue(0, 0);
  volumeActor
    .getProperty()
    .setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05);
  // - Use shading based on gradient
  volumeActor.getProperty().setShade(true);
  volumeActor.getProperty().setUseGradientOpacity(0, true);
  // - generic good default
  volumeActor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
  volumeActor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
  volumeActor.getProperty().setAmbient(0.2);
  volumeActor.getProperty().setDiffuse(0.7);
  volumeActor.getProperty().setSpecular(0.3);
  volumeActor.getProperty().setSpecularPower(8.0);

  renderer.resetCamera();
  renderWindow.render();
}

// ----------------------------------------------------------------------------
// Use a file reader to load a local file
// ----------------------------------------------------------------------------
const load_model = document.querySelector('#load_model');
const load_volume = document.querySelector('#load_volume');

function handle_model(event) {
  event.preventDefault();
  const dataTransfer = event.dataTransfer;
  const files = event.target.files || dataTransfer.files;
  if (files.length === 1) {
    const fileReader = new FileReader();
    fileReader.onload = function onLoad(e) {
      reader.parseAsArrayBuffer(fileReader.result);
      update();
    };
    fileReader.readAsArrayBuffer(files[0]);
  }
}

function handle_volume(event) {
  event.preventDefault();
  const dataTransfer = event.dataTransfer;
  const files = event.target.files || dataTransfer.files;
  if (files.length === 1) {
    console.log(files);
    console.log(files[0].name);
    //fileReader.readAsArrayBuffer(files[0]);
    const fileReader = new FileReader();
    fileReader.onload = function onLoad(e) {
      create_volume(fileReader.result);
    };
  }
}

load_model.addEventListener('change', handle_model);
load_volume.addEventListener('change', handle_volume);
