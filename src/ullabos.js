var data = {
  xml_version: "1.0",
  fullDomain: [0.0, 10.0, 0.0, 10.0, 0.0, 10.0],
  resolution: 64,
  smooth: 0,
  levels: 1,
  domains:[]
};


module.exports = {
  update_data_tmp: () => {
    var json = JSON.stringify(data);
    $("#tmp").html("<pre>"+json+"</pre>")
  },

  update_data: (param, value) => {

    value = parseInt(value);

    switch (param) {

      case 'resolution':
      data.resolution = value;
      break;

      case 'smooth':
      data.smooth = value;
      break;

      //full domain
      case 'fdx0':
      data.fullDomain[0] = value;
      break;

      case 'fdx1':
      data.fullDomain[1] = value;
      break;

      case 'fdy0':
      data.fullDomain[2] = value;
      break;

      case 'fdy1':
      data.fullDomain[3] = value;
      break;

      case 'fdz0':
      data.fullDomain[4] = value;
      break;

      case 'fdz1':
      data.fullDomain[5] = value;
      break;
    }

  },


  add_domain: (level_id, coords, callback) => {

    normalize_domain(level_id, coords, function(domain){

      data.domains.push(domain);

      callback(data.domains.length);
    })
  },

  remove_domain: (domain_id) => {
    data.domains.splice(domain_id, 1);
  },


  add_level: () => {
    data.levels++;
  },

  remove_level: (level_id) => {
    data.levels--;
  },

  get_data: (callback) => {
    prepare_data(function(prepared_data){
      callback(prepared_data);
    })
  },

  get_levels_nb: (callback) => {
    callback(data.levels);
  },

  get_level_domains: (id, callback) => {
    var ids = [];
    for (var i = 0; i < data.domains.length; i++) {
      if(data.domains[i].level == id){
        ids.push(i);
      }
    }

    callback(ids);
  }
}

function normalize_domain(level_id, domain, callback){
  callback({"level": level_id + 1, "x0": domain[0], "x1": domain[1], "y0": domain[2], "y1": domain[3], "z0": domain[4], "z1": domain[5]})
}

function find_level_domains(level_id, callback){

}


function prepare_data(callback){
  var prepared_data = Object.assign({}, data);
  var fdomain = JSON.stringify(data.fullDomain);
  // remove commas, first and last characters
  prepared_data.fullDomain = fdomain.replaceAll(",", " ").slice(1,-1)

  var domains = [[]];

  for (var i = 0; i < data.domains.length; i++) {
    var d = data.domains[i];

    domains[d.level-1].push([d.x0, d.x1, d.y0, d.y1, d.z0, d.z1]);

    var tmp = JSON.stringify(domains[d.level-1][i]);
    domains[d.level-1][i] = tmp.replaceAll(",", " ").slice(1,-1);
  }

  prepared_data.domains = domains;

console.log(prepared_data);

  callback(prepared_data);
}
