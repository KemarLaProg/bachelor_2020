module.exports = {

  show_alert: (alert_nb, msg) => {
    var type;
    switch (alert_nb) {
      case 0:
      type = "warning";
      break;

      case 1:
      type = "success";
      break;

      default:
      type = "warning"
    }

    $('#advert .alert').removeClass('.alert-* d-none');

    $('#advert .alert').addClass('alert-' + type + ' d-block').html(msg);

    setTimeout(function (){
      $('#advert .alert').removeClass('alert-' + type + ' d-block');

      $('#advert .alert').addClass('.alert-* d-none');
    }, 3000)
  },

  remove_alert: (type) => {

    $('#advert .alert').removeClass('alert-' + type + ' d-block');

    $('#advert .alert').addClass('.alert-* d-none');
  }
}
