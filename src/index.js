import * as u from './ullabos'
import * as alert from './alert'
import * as v from './vtk'


$("#fdx0").change(function(){
  v.update_fulldomain("x0", parseInt(this.value), parseInt($("#fdx1").val()));
});
$("#fdx1").change(function(){
  v.update_fulldomain("x1", parseInt($("#fdx0").val()), parseInt(this.value));
});
$("#fdy0").change(function(){
  v.update_fulldomain("y0", parseInt(this.value), parseInt($("#fdy1").val()));
});
$("#fdy1").change(function(){
  v.update_fulldomain("y1", parseInt($("#fdy0").val()), parseInt(this.value));
});
$("#fdz0").change(function(){
  v.update_fulldomain("z0", parseInt(this.value), parseInt($("#fdz1").val()));
});
$("#fdz1").change(function(){
  v.update_fulldomain("z1", parseInt($("#fdz0").val()), parseInt(this.value));
});



export function add_domain(){
  var level_id = parseInt($('#dlevel').val());

  var domain = [parseInt($('#dx0').val()), parseInt($('#dx1').val()), parseInt($('#dy0').val()), parseInt($('#dy1').val()), parseInt($('#dz0').val()), parseInt($('#dz1').val())]

  u.add_domain(level_id - 1, domain, function(domain_id){

    domain_id--;
    var ld = "l" + level_id + "d" + domain_id;

    $("#level-" + level_id).append(`
      <div class="domain" id="${ld}">
      <div class="row">
      <div class="col">x: ${domain[0]} ${domain[1]}</div>
      <div class="col">y: ${domain[2]} ${domain[3]}</div>
      <div class="col">z: ${domain[4]} ${domain[5]}</div>
      </div>

      <div class="row">
      <div class="col">
      <button type="button" class="btn" value="${ld}" onclick="MyLibrary.toggle_box_visibility(this, this.value)">Hide</button>
      </div>

      <div class="col text-right">
      <button type="button" class="btn" value="${ld}" onclick="MyLibrary.remove_domain(this.value)">X</button>
      </div>
      </div>
      </div>`
    );

    alert.show_alert(1, "Domain successfully added");

    u.update_data_tmp()

    v.add_box(domain);
  });
}

export function remove_domain(html_id){
  translate_id(html_id, function(level_id, domain_id){

    u.remove_domain(level_id, domain_id)

    $("#" + html_id).remove();
    v.remove_box(domain_id);

    alert.show_alert(1, "Domain successfully removed");

    u.update_data_tmp();
  })
}

export function add_level(){

  u.get_levels_nb(function(level_nb){
    var level_id = level_nb + 1;

    $('#dlevel').attr({"max": level_id});

    $("#level-list").append(`
      <div class="level w-100" id="level-${level_id}">
      <div class="row level-title">
      <div class="col">
      <p>Level ${level_id}</p>
      </div>
      <div class="col text-right">
      <button type="button" class="btn" value="${level_id}" onclick="MyLibrary.remove_level(${level_id})">X</button>
      </div>
      </div>`
    );

    u.add_level();

    alert.show_alert(1, "Level " + level_id +" successfully added")

    u.update_data_tmp()

  })
}

export function remove_level(level_id){

  console.log("level id: " + level_id);

  u.get_level_domains(level_id, function(domains){

    console.log(domains);
    domains = domains.reverse();

    for (var i = 0; i < domains.length; i++) {
      v.remove_box(domains[i]);
    }

    $("#level-" + level_id).remove()

    alert.show_alert(1, "Level " + level_id +" successfully removed")

    u.update_data_tmp()

  })
}

export function update_data(param, value){
  u.update_data(param, value)

  u.update_data_tmp()
}

export function toggle_level_visibility(btn, html_id){

}

export function toggle_box_visibility(btn, html_id){
  translate_id(html_id, function(level_id, domain_id){

    v.toggle_box_visibility(domain_id, function(visible){
      if(visible){
        $(btn).html("Hide");

        alert.show_alert(1, "Domain successfully shown")

      }else {
        $(btn).html("Show");

        alert.show_alert(1, "Domain successfully hidden")
      }
    });
  });

}

export function generate_xml(callback){
  u.get_data(function(data){
    callback(data);
  });
}

export function show_alert(id, msg){
  alert.show_alert(id, msg);
}

function translate_id(html_id, callback){
  var n = html_id.match(/\d+/g);

  callback(n[0] - 1, n[1]);
}
